package networking

import (
	"doppler/networking/commands"
	"net"
)

type NetworkingMode uint8

func StartNetworking(networkingMode NetworkingMode) {
	switch networkingMode {
	case Client:
		StartClient()
	case Node:
		StartNodeServer()
	default:
		StartClient()
	}

}

type Peer struct {
	Connection *net.Conn
	Type       PeerType
}

type PeerSignal struct {
	Command commands.PeerCommand
	Peer    *Peer
}

const (
	Client NetworkingMode = iota
	Node
)

type PeerType int

const (
	Remote = iota
	Local
)
