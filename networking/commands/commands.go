package commands

import (
	"fmt"

	"github.com/vmihailenco/msgpack/v5"
)

type NodeCommandType byte

type ISerializeable interface {
	Serialize() []byte
	Deserialize([]byte)
}

type NetworkCommand struct {
	CommandType NodeCommandType
	Command     []byte
}

const (
	GetAvailablePeers NodeCommandType = iota
	GetAvailablePeersResult

	InitializePunchingWithClient

	Handshake

	PunchingSignal
)

func (nc *NetworkCommand) Serialize() []byte {
	b, err := msgpack.Marshal(&nc)
	if err != nil {
		fmt.Println(err)
	}
	return b
}

func (nc *NetworkCommand) Deserialize(bytes []byte) {
	err := msgpack.Unmarshal(bytes, nc)
	if err != nil {
		fmt.Println(err)
	}
}
