package commands

import (
	"fmt"

	"github.com/vmihailenco/msgpack/v5"
)

type PeerCommand struct {
	Command     []byte
	CommandType PeerCommandType
}

type PeerCommandType int16

func (nc *PeerCommand) Serialize() []byte {
	b, err := msgpack.Marshal(&nc)
	if err != nil {
		fmt.Println(err)
	}
	return b
}

func (nc *PeerCommand) Deserialize(bytes []byte) {
	err := msgpack.Unmarshal(bytes, nc)
	if err != nil {
		fmt.Println(err)
	}
}

const (
	PushBlock = iota
)
