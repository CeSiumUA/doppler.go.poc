package nodecommands

import (
	"doppler/networking/commands"
	"fmt"

	"github.com/vmihailenco/msgpack/v5"
)

type PunchingSignalCommand struct {
	RemoteAddress string
}

func (nc *PunchingSignalCommand) Serialize() []byte {
	b, err := msgpack.Marshal(&nc)
	if err != nil {
		fmt.Println(err)
	}
	command := commands.NetworkCommand{
		CommandType: commands.PunchingSignal,
		Command:     b,
	}
	bc := command.Serialize()
	return bc
}

func (nc *PunchingSignalCommand) Deserialize(bytes []byte) {
	err := msgpack.Unmarshal(bytes, nc)
	if err != nil {
		fmt.Println(err)
	}
}
