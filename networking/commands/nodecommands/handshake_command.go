package nodecommands

import (
	"doppler/networking/commands"
	"fmt"

	"github.com/vmihailenco/msgpack/v5"
)

type HandshakeCommand struct {
}

func NewHandshakeCommand() HandshakeCommand {
	return HandshakeCommand{}
}

func (nc *HandshakeCommand) Serialize() []byte {
	b, err := msgpack.Marshal(&nc)
	if err != nil {
		fmt.Println(err)
	}
	command := commands.NetworkCommand{
		CommandType: commands.Handshake,
		Command:     b,
	}
	bc := command.Serialize()
	return bc
}

func (nc *HandshakeCommand) Deserialize(bytes []byte) {
	err := msgpack.Unmarshal(bytes, nc)
	if err != nil {
		fmt.Println(err)
	}
}
