package nodecommands

import (
	"doppler/networking/commands"
	"fmt"

	"github.com/vmihailenco/msgpack/v5"
)

type InitializePunchinghWithClientCommand struct {
	Address string
}

func NewInitializePunchingWithClientCommand(remoteClientAddress string) InitializePunchinghWithClientCommand {
	return InitializePunchinghWithClientCommand{
		Address: remoteClientAddress,
	}
}

func (nc *InitializePunchinghWithClientCommand) Serialize() []byte {
	b, err := msgpack.Marshal(&nc)
	if err != nil {
		fmt.Println(err)
	}
	command := commands.NetworkCommand{
		CommandType: commands.InitializePunchingWithClient,
		Command:     b,
	}
	bc := command.Serialize()
	return bc
}

func (nc *InitializePunchinghWithClientCommand) Deserialize(bytes []byte) {
	err := msgpack.Unmarshal(bytes, nc)
	if err != nil {
		fmt.Println(err)
	}
}
