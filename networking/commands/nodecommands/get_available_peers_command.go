package nodecommands

import (
	"doppler/networking/commands"
	"fmt"

	"github.com/vmihailenco/msgpack/v5"
)

type GetAvailablePeersCommand struct {
}

func NewGetAvailablePeersCommand() GetAvailablePeersCommand {
	return GetAvailablePeersCommand{}
}

func (nc *GetAvailablePeersCommand) Serialize() []byte {
	b, err := msgpack.Marshal(&nc)
	if err != nil {
		fmt.Println(err)
	}
	command := commands.NetworkCommand{
		CommandType: commands.GetAvailablePeers,
		Command:     b,
	}
	bc := command.Serialize()
	return bc
}

func (nc *GetAvailablePeersCommand) Deserialize(bytes []byte) {
	err := msgpack.Unmarshal(bytes, nc)
	if err != nil {
		fmt.Println(err)
	}
}
