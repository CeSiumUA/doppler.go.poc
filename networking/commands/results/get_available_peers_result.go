package results

import (
	"doppler/networking/commands"
	"fmt"

	"github.com/vmihailenco/msgpack/v5"
)

type GetAvailablePeersCommandResult struct {
	Peers []string
}

func NewGetAvailablePeersCommandResult() GetAvailablePeersCommandResult {
	return GetAvailablePeersCommandResult{}
}

func (nc *GetAvailablePeersCommandResult) Serialize() []byte {
	b, err := msgpack.Marshal(&nc)
	if err != nil {
		fmt.Println(err)
	}
	command := commands.NetworkCommand{
		CommandType: commands.GetAvailablePeersResult,
		Command:     b,
	}
	bc := command.Serialize()
	return bc
}

func (nc *GetAvailablePeersCommandResult) Deserialize(bytes []byte) {
	err := msgpack.Unmarshal(bytes, nc)
	if err != nil {
		fmt.Println(err)
	}
}
