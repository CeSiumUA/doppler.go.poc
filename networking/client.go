package networking

import (
	"doppler/networking/commands"
	"doppler/networking/commands/nodecommands"
	"doppler/networking/commands/results"
	"fmt"
	"net"
	"strings"
	"time"

	"github.com/libp2p/go-reuseport"
	"github.com/mostlygeek/arp"
)

const nodeAddress = "5.189.145.4:13768"
const localListenerPort = ":4838"

var handShakeChannel chan *nodecommands.HandshakeCommand = make(chan *nodecommands.HandshakeCommand)
var availablePeersChannel chan *nodecommands.GetAvailablePeersCommand = make(chan *nodecommands.GetAvailablePeersCommand)
var availablePeersResultChannel chan *results.GetAvailablePeersCommandResult = make(chan *results.GetAvailablePeersCommandResult)

var initializePunchingChannel chan *nodecommands.InitializePunchinghWithClientCommand = make(chan *nodecommands.InitializePunchinghWithClientCommand)
var punchSignalChannel chan *nodecommands.PunchingSignalCommand = make(chan *nodecommands.PunchingSignalCommand)

var peerCommandChannel chan PeerSignal = make(chan PeerSignal)

var networkClientWriterChannel chan []byte = make(chan []byte)

var peerConnections []*Peer = make([]*Peer, 0)

var mainNodeConnection *net.Conn

func StartClient() {
	getNodeConnection()
	go startLocalListener()
	go connectToLocalPeers()
	go networkWriter()
	go networkReader()
	go punchingSignalProcessor()
	peers := getPeers()
	connectToPeers(peers)
}

func punchingSignalProcessor() {
	for {
		signal := <-punchSignalChannel
		localAddressString := (*mainNodeConnection).LocalAddr().String()
		err := (*mainNodeConnection).Close()
		if err != nil {
			Output("Close TCP error:", err)
		}
		Output("Got punching signal, to:", signal.RemoteAddress)
		punch(localAddressString, signal.RemoteAddress)
	}
}

func connectToPeers(peers []string) {
	Output("Connecting to peers...")
	for _, peer := range peers {
		Output("Punching peer:", peer)
		punchingCommand := nodecommands.NewInitializePunchingWithClientCommand(peer)
		networkClientWriterChannel <- punchingCommand.Serialize()
	}
}

func punch(localAddressString, remoteAddress string) {

	localAddress := ":" + strings.Split(localAddressString, ":")[1]
	Output("Connecting to", remoteAddress, "from", localAddress)
	go startPeerListener(":" + strings.Split(localAddressString, ":")[1])
	conn, err := reuseport.Dial("tcp", localAddressString, remoteAddress)
	if err != nil {
		Output("Dial to peer error:", err)
	} else {
		peer := Peer{
			Connection: &conn,
			Type:       Remote,
		}
		addConnection(&peer)
		Output("Connection succeeded! Peer added to peers pool")
	}
	defer func() {
		getNodeConnection()
	}()
}

func getNodeConnection() {
	conn, err := net.Dial("tcp", nodeAddress)
	if err != nil {
		Output("Node dial error:", err)
		panic(err)
	}
	Output("Connected to node on:", conn.LocalAddr().String())

	mainNodeConnection = &conn
}

func getPeers() []string {
	getPeersCommand := nodecommands.NewGetAvailablePeersCommand()

	commandBytes := getPeersCommand.Serialize()

	networkClientWriterChannel <- commandBytes

	getPeersCommandResult := <-availablePeersResultChannel

	peers := (*getPeersCommandResult).Peers

	return peers
}

func networkWriter() {
	for {
		bytesToWrite := <-networkClientWriterChannel
		if len(bytesToWrite) > 0 {
			writtenBytes, err := (*mainNodeConnection).Write(bytesToWrite)
			if err != nil {
				Output("Network writer error:", err)
				continue
			}
			Output("Wrote", writtenBytes, "bytes to channel!")
		} else {
			Output("Channel empty!")
		}
	}
}

func networkReader() {
	for {
		buffer := make([]byte, 8192)
		readBytes, err := (*mainNodeConnection).Read(buffer)
		if err != nil {
			Output("Network reader error:", err)
			time.Sleep(5 * time.Second)
			continue
		} else if readBytes == 0 && len(buffer) == 0 {
			Output("Buffer is empty")
			continue
		}
		Output("Received", readBytes, "bytes")
		go processBytesToCommand(buffer[0:readBytes])
	}
}

func processBytesToCommand(bytes []byte) {
	networkCommand := commands.NetworkCommand{}
	networkCommand.Deserialize(bytes)
	switch networkCommand.CommandType {
	case commands.GetAvailablePeersResult:
		getPeersResult := results.NewGetAvailablePeersCommandResult()
		getPeersResult.Deserialize(networkCommand.Command)
		availablePeersResultChannel <- &getPeersResult
	case commands.PunchingSignal:
		punchingSignal := nodecommands.PunchingSignalCommand{}
		punchingSignal.Deserialize(networkCommand.Command)
		punchSignalChannel <- &punchingSignal
	}
}

func Output(a ...interface{}) {
	fmt.Println(a...)
}

func startPeerListener(listeningEndpoint string) {
	Output("Starting listener on:", listeningEndpoint)
	listener, err := reuseport.Listen("tcp", listeningEndpoint)
	if err != nil {
		Output("Listener error:", err)
		return
	}
	conn, err := listener.Accept()
	if err != nil {
		Output("Client accept error:", err)
		return
	}
	Output("Got connection from ", conn.RemoteAddr().String())
}

func startLocalListener() {
	ipAddr, err := net.ResolveTCPAddr("tcp", localListenerPort)
	if err != nil {
		fmt.Println("Error parsing local address string:", ipAddr)
	}
	listener, err := net.ListenTCP("tcp", ipAddr)
	if err != nil {
		fmt.Println("Error starting local server:", err)
	}
	for {
		connection, err := listener.Accept()
		if err != nil {
			fmt.Println("Error accepting peer connection:", err)
			continue
		}
		peer := Peer{
			Connection: &connection,
			Type:       Local,
		}
		addConnection(&peer)
		fmt.Println("Connection from", connection.RemoteAddr())
	}
}

func connectToLocalPeers() {
	for ip, _ := range arp.Table() {
		go func(address string) {
			conn, err := net.Dial("tcp", fmt.Sprintf("%s%s", address, localListenerPort))
			if err != nil {
				fmt.Println("Error connecting to local address:", address)
				return
			}
			peer := Peer{
				Connection: &conn,
				Type:       Local,
			}
			addConnection(&peer)
			fmt.Println("Successfully connected to peer", conn.RemoteAddr())
		}(ip)
	}
}

func addConnection(peer *Peer) {
	peerConnections = append(peerConnections, peer)
	go func(pr *Peer) {
		for {
			conn := peer.Connection
			buffer := make([]byte, 8192)
			bytesReaded, err := (*conn).Read(buffer)
			if err != nil {
				return
			}
			peerCommand := commands.PeerCommand{}
			peerCommand.Deserialize(buffer[0:bytesReaded])
			peerSignal := PeerSignal{
				Command: peerCommand,
				Peer:    pr,
			}
			peerCommandChannel <- peerSignal
		}
	}(peer)
}

func SubscribeToPeersMessages(reader ReaderDelegate) {
	for {
		peerSignal := <-peerCommandChannel
		go reader(peerSignal)
	}
}

func SendToSinglePeer(peerSignal *PeerSignal) (int, error) {
	bytes := peerSignal.Command.Serialize()
	sentCount, err := (*peerSignal.Peer.Connection).Write(bytes)
	return sentCount, err
}

func Broadcast(peerCommand *commands.PeerCommand) {
	bytes := peerCommand.Serialize()
	for _, peer := range peerConnections {
		(*peer.Connection).Write(bytes)
	}
}

type ReaderDelegate func(PeerSignal)
