package networking

import (
	"doppler/networking/commands"
	"doppler/networking/commands/nodecommands"
	"doppler/networking/commands/results"
	"net"
)

type GetAvailablePeersClientCommand struct {
	Conection *net.TCPConn
	Command   *nodecommands.GetAvailablePeersCommand
}

type InitializePunchingClientCommand struct {
	Connection *net.TCPConn
	Command    *nodecommands.InitializePunchinghWithClientCommand
}

var connectedClients []*net.TCPConn = make([]*net.TCPConn, 0)

var availablePeersNodeChannel chan *GetAvailablePeersClientCommand = make(chan *GetAvailablePeersClientCommand)
var initializePunchingWithClientNodeChannel chan *InitializePunchingClientCommand = make(chan *InitializePunchingClientCommand)

func StartNodeServer() {
	go startListener()
	go getAvailablePeersCommandProcessor()
	go initializePunchingCommandProcessor()
}

func startListener() {
	localAddress, err := net.ResolveTCPAddr("tcp", "0.0.0.0:13768")
	if err != nil {
		Output(err)
	}
	listener, err := net.ListenTCP("tcp", localAddress)
	if err != nil {
		Output(err)
	}
	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			Output(err)
		}
		connectedClients = append(connectedClients, conn)
		go clientNetworkReader(conn)
	}
}

func clientNetworkReader(conn *net.TCPConn) {
	for {
		buffer := make([]byte, 8192)
		readLength, err := conn.Read(buffer)
		if err != nil {
			Output("Error reading client stream!", err)
			removeClient(conn)
			break
		} else if readLength == 0 && len(buffer) == 0 {
			Output("Buffer is empty")
			continue
		}
		Output("Received", readLength, "bytes")
		go processNodeBytesToCommand(buffer[0:readLength], conn)
	}
}

func getAvailablePeersCommandProcessor() {
	for {
		command := <-availablePeersNodeChannel
		Output("Got Available peers command!")
		conn := command.Conection
		peers := make([]string, 0)
		Output("Peers:", connectedClients)
		for _, peer := range connectedClients {
			peerStr := peer.RemoteAddr().String()
			if peerStr != conn.RemoteAddr().String() {
				peers = append(peers, peerStr)
			}
		}
		Output("Peers to send:", peers)
		result := results.GetAvailablePeersCommandResult{
			Peers: peers,
		}
		serializedPeers := result.Serialize()
		conn.Write(serializedPeers)
	}
}

func initializePunchingCommandProcessor() {
	for {
		command := <-initializePunchingWithClientNodeChannel
		peer := command.Command.Address

		senderPunchingSignal := nodecommands.PunchingSignalCommand{
			RemoteAddress: peer,
		}

		command.Connection.Write(senderPunchingSignal.Serialize())

		for _, client := range connectedClients {
			if peer == client.RemoteAddr().String() {
				punchingSignal := nodecommands.PunchingSignalCommand{
					RemoteAddress: command.Connection.RemoteAddr().String(),
				}
				client.Write(punchingSignal.Serialize())
			}
		}
	}
}

func processNodeBytesToCommand(bytes []byte, conn *net.TCPConn) {
	networkCommand := commands.NetworkCommand{}
	networkCommand.Deserialize(bytes)
	Output("Incomming command type:", networkCommand.CommandType)
	switch networkCommand.CommandType {
	case commands.GetAvailablePeers:
		getPeersCommand := nodecommands.GetAvailablePeersCommand{}
		getPeersCommand.Deserialize(networkCommand.Command)
		Output("Deserialized client command!")
		comm := GetAvailablePeersClientCommand{
			Conection: conn,
			Command:   &getPeersCommand,
		}
		availablePeersNodeChannel <- &comm
	case commands.InitializePunchingWithClient:
		initializePunchingCommand := nodecommands.InitializePunchinghWithClientCommand{}
		initializePunchingCommand.Deserialize(networkCommand.Command)
		Output("Deserialized client command!")
		comm := InitializePunchingClientCommand{
			Connection: conn,
			Command:    &initializePunchingCommand,
		}
		initializePunchingWithClientNodeChannel <- &comm
	}
}

func removeClient(conn *net.TCPConn) {
	connections := make([]*net.TCPConn, 0)
	for _, peer := range connectedClients {
		if conn.RemoteAddr().String() != peer.RemoteAddr().String() {
			connections = append(connections, peer)
		}
	}
	connectedClients = connections
}
