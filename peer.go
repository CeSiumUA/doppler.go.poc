package main

import (
	"doppler/networking"
	"fmt"
)

func StartPeer() {
	fmt.Println("Starting as client...")
	networking.StartNetworking(networking.Client)
}
