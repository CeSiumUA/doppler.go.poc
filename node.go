package main

import (
	"doppler/networking"
	"fmt"
)

func StartNode() {
	fmt.Println("Starting as node...")
	networking.StartNetworking(networking.Node)
	networking.SubscribeToPeersMessages(NetworkReader)
}

func NetworkReader(peerSignal networking.PeerSignal) {
}
