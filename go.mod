module doppler

go 1.17

require github.com/google/uuid v1.3.0

require (
	github.com/libp2p/go-reuseport v0.1.0 // indirect
	github.com/mostlygeek/arp v0.0.0-20170424181311-541a2129847a // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/sys v0.0.0-20190228124157-a34e9553db1e // indirect
)
