package blockcrypto

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/gob"
	"fmt"
	"time"

	"github.com/google/uuid"
)

type Block struct {
	TransactionsList []*Transaction
	PreviousHash     hex
	Nonce            hex
	Difficulty       int8
	Id               hex
	TimeStamp        int64
}

func CreateBlock(transactions []*Transaction, previousHash hex) *Block {
	id := uuid.NewString()
	timestamp := time.Now().UTC().Unix()
	blck := Block{
		Id:               hex(id),
		TimeStamp:        timestamp,
		TransactionsList: transactions,
		PreviousHash:     previousHash,
	}
	return &blck
}

func (blck *Block) Mine() {
	blck.MineWithDifficulty(2)
}

func (blck *Block) MineWithDifficulty(difficulty int8) {
	blck.Difficulty = difficulty
	var tries int64 = 0
	for {
		randomNonce := make([]byte, 8)
		rand.Read(randomNonce)
		blck.Nonce = randomNonce
		difficultyProofed := blck.IsDifficultyProofed()
		tries++
		if difficultyProofed {
			break
		}
	}
	println(tries)
}

func (blck *Block) IsDifficultyProofed() bool {
	difficulty := blck.Difficulty
	hash := blck.CreateHash()
	hashSlice := hash[0:difficulty]
	for _, b := range hashSlice {
		if b != 0 {
			return false
		}
	}
	return true
}

func (blck *Block) CreateHash() [32]byte {
	serialized := blck.Serialize()
	return sha256.Sum256(serialized)
}

func (blck *Block) Serialize() hex {
	b := bytes.Buffer{}
	e := gob.NewEncoder(&b)
	err := e.Encode(blck)
	if err != nil {
		fmt.Println("Failed to encode a block", err)
	}
	return b.Bytes()
}
