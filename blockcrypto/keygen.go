package blockcrypto

import (
	"crypto/rand"
	"crypto/rsa"
)

type hex []byte

func GenerateKey() *rsa.PrivateKey {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		println("Error generating privateKey")
	}
	privateKey.Precompute()
	return privateKey
}
