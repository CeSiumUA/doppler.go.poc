package blockcrypto

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"math"
)

type BlockNexus struct {
	Block         *Block
	PreviousBlock *BlockNexus
}

func (blockNexus *BlockNexus) GetLength() int64 {
	var lastBlock *BlockNexus = blockNexus
	var count int64 = 0
	for {
		lastBlock = lastBlock.PreviousBlock
		count++
		if lastBlock == nil || lastBlock.Block == nil {
			break
		}
	}
	return count
}

func (blockNexus *BlockNexus) GetComplexity() int64 {
	lastBlock := blockNexus
	var complexity int64 = 0
	for {
		complexity += int64(math.Pow(2, float64(lastBlock.Block.Difficulty)))
		lastBlock = lastBlock.PreviousBlock
		if lastBlock == nil || lastBlock.Block == nil {
			break
		}
	}
	return complexity
}

func (blockNexus *BlockNexus) Serialize() hex {
	b := bytes.Buffer{}
	e := gob.NewEncoder(&b)
	err := e.Encode(blockNexus)
	if err != nil {
		fmt.Println("Failed to encode a block", err)
	}
	return b.Bytes()
}

func DeserializeToBlockNexus(rawBytes hex) *BlockNexus {
	b := bytes.NewBuffer(rawBytes)
	d := gob.NewDecoder(b)
	blockNexus := new(BlockNexus)
	err := d.Decode(blockNexus)
	if err != nil {
		println(err)
	}
	return blockNexus
}
