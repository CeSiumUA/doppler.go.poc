package blockcrypto

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"

	"github.com/google/uuid"
)

type Transaction struct {
	From      hex
	To        hex
	Content   hex
	Metadata  hex
	Signature hex
	Id        hex
}

func CreateTransaction(from hex, to hex, content hex, metadata hex) *Transaction {
	id := uuid.NewString()
	trans := Transaction{
		From:     from,
		To:       to,
		Content:  content,
		Metadata: metadata,
		Id:       []byte(id),
	}
	return &trans
}

func (trans *Transaction) Sign(privateKey *rsa.PrivateKey) {
	hash := trans.CreateHash()
	rng := rand.Reader
	signature, error := rsa.SignPKCS1v15(rng, privateKey, crypto.SHA256, hash[:])
	if error != nil {
		println("Error signing transaction!!!")
	}
	trans.Signature = signature
}

func (trans Transaction) Verify(publicKey *rsa.PublicKey) error {
	transactionHash := trans.CreateHash()
	return rsa.VerifyPKCS1v15(publicKey, crypto.SHA256, transactionHash[:], trans.Signature)
}

func (trans *Transaction) CreateHash() [32]byte {
	summary := trans.Content
	summary = append(summary, trans.From...)
	summary = append(summary, trans.To...)
	summary = append(summary, trans.Id...)
	return sha256.Sum256(summary)
}
