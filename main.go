package main

import (
	"fmt"
	"os"
)

func main() {
	cmdArgs := os.Args[1:]

	useAsClient := true

	fmt.Println(cmdArgs)
	if len(cmdArgs) > 0 {
		mode := cmdArgs[0]
		if mode == "node" {
			useAsClient = false
		}
	}
	if useAsClient {
		StartPeer()
	} else {
		StartNode()
	}

	fmt.Scanln()
}

/*key := blockcrypto.GenerateKey()
newTransaction := blockcrypto.CreateTransaction(key.PublicKey.N.Bytes(), key.PublicKey.N.Bytes(), []byte("This is a test message to send"), make([]byte, 0))
newTransaction.Sign(key)
transactions := [1]*blockcrypto.Transaction{newTransaction}

block1 := blockcrypto.CreateBlock(transactions[:], make([]byte, 0))
block1.Mine()
block1Hash := block1.CreateHash()
block1Nexus := blockcrypto.BlockNexus{
	Block:         block1,
	PreviousBlock: nil,
}

block2 := blockcrypto.CreateBlock(transactions[:], block1Hash[:])
block2.Mine()
block2Hash := block2.CreateHash()
block2Nexus := blockcrypto.BlockNexus{
	Block:         block2,
	PreviousBlock: &block1Nexus,
}

block3 := blockcrypto.CreateBlock(transactions[:], block2Hash[:])
block3.Mine()
block3Nexus := blockcrypto.BlockNexus{
	Block:         block3,
	PreviousBlock: &block2Nexus,
}

cmplx := block3Nexus.GetComplexity()
lngth := block3Nexus.GetLength()
serialized := block3Nexus.Serialize()
deserialized := blockcrypto.DeserializeToBlockNexus(serialized)
fmt.Printf("%d , %d, %b", cmplx, lngth, deserialized.Block.PreviousHash)*/
