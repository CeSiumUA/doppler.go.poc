import { Component } from '@angular/core';
import { DataproviderService } from '../services/dataprovider.service';
import { IListChat } from '../../models/IListChat';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'doppler';
  constructor(private dataProviderService: DataproviderService){

  }

  public getChats(): IListChat[]{
    return this.dataProviderService.getChats();
  }
}
