import { Injectable } from '@angular/core';
import { IListChat } from '../../models/IListChat';

@Injectable({
  providedIn: 'root'
})
export class DataproviderService {

  constructor() { }

  public getChats(): IListChat[]{
    var listChat1: IListChat = {
      Name: 'Test',
      Icon: 'favicon.ico'
    }
    return [listChat1]
  }
}
